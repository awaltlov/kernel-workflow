"""Tests for the session_events module."""
from datetime import datetime
from importlib import resources
import typing
from unittest import TestCase
from unittest import mock

from cki_lib.yaml import load as cki_yaml_load

from tests import fakes
from webhook import session_events
from webhook.common import get_arg_parser
from webhook.session import SessionRunner

AMQP_HEADERS = {'message-type': 'amqp-bridge'}
GL_HEADERS = {'message-type': 'gitlab'}
JIRA_HEADERS = {'message-type': 'jira'}
UMB_BRIDGE_HEADERS = {'message-type': 'cki.kwf.umb-bz-event'}

MOCK_GL_USERNAME = 'botty'


def load_json_from_asset(path: str, module: str = 'tests.assets.webhook_events') -> dict | list:
    """Return the json or yaml asset contents."""
    return cki_yaml_load(file_path=resources.files(module).joinpath(path))


def create_session(hook_name: str, args: str = '') -> SessionRunner:
    """Return a new SessionRunner with a fake gl_instance."""
    parsed_args = get_arg_parser('TEST').parse_args(args.split())
    session = SessionRunner(hook_name, parsed_args, {})
    session.gl_instance = fakes.FakeGitLab()
    session.gl_instance.auth()
    return session


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestGLEvent(TestCase):
    """Tests for the GL Events."""

    def test_gl_merge_request_event_1(self) -> None:
        """Properties have the expected values."""
        test_session = create_session('bughook')
        body = load_json_from_asset('gl_merge_request_1.json')
        test_event = session_events.create_event(test_session, GL_HEADERS, body)

        # GitlabEvent properties
        self.assertEqual(test_event.filters, session_events.GITLAB_MR_EVENT_FILTERS)
        self.assertEqual(test_event.triggers, session_events.GITLAB_MR_EVENT_TRIGGERS)
        self.assertIsInstance(test_event.created_at, datetime)
        self.assertEqual(test_event.draft_status, (False, False))
        self.assertEqual(test_event.user, 'cki-kwf-bot')
        self.assertIs(test_event.kind, session_events.GitlabObjectKind.MERGE_REQUEST)
        self.assertFalse(test_event.mr_inactive)
        self.assertEqual(test_event.merge_request, body['object_attributes'])
        self.assertEqual(len(test_event.labels), 20)
        self.assertTrue(all(isinstance(label, session_events.Label) for label in test_event.labels))
        self.assertEqual(test_event.project, body['project'])
        self.assertEqual(test_event.namespace, 'redhat/centos-stream/src/kernel/centos-stream-9')
        self.assertEqual(test_event.mr_url, 'https://gitlab.com/redhat/centos-stream/'
                                            'src/kernel/centos-stream-9/-/merge_requests/2756')
        self.assertIsInstance(test_event.mr_url, session_events.GitlabURL)
        self.assertEqual(test_event.bot_users, {fakes.AUTH_USER['username']})

        # GitlabMREvent properties
        self.assertEqual(test_event.action, 'update')
        self.assertEqual(test_event.changes, body['changes'])
        self.assertFalse(test_event.closing)
        self.assertFalse(test_event.commits_changed)

        # BaseEvent properties
        self.assertIs(test_event.type, session_events.MessageType.GITLAB)
        self.assertEqual(test_event.rh_project.id, 11223344)
        self.assertTrue(test_event.matches_environment)
        self.assertTrue(test_event.filter_bots)
        self.assertTrue(test_event.passes_filters)
        self.assertFalse(test_event.matches_trigger)

    def test_gl_note_event_1(self) -> None:
        """Properties have the expected values."""
        test_session = create_session('ack_nack')
        body = load_json_from_asset('gl_note_1.json')
        test_event = session_events.create_event(test_session, GL_HEADERS, body)

        # GitlabEvent properties
        self.assertEqual(test_event.filters, session_events.GITLAB_NOTE_EVENT_FILTERS)
        self.assertEqual(test_event.triggers, session_events.GITLAB_NOTE_EVENT_TRIGGERS)
        self.assertIsInstance(test_event.created_at, datetime)
        self.assertEqual(test_event.draft_status, (False, None))
        self.assertEqual(test_event.user, 'ptalbert')
        self.assertIs(test_event.kind, session_events.GitlabObjectKind.NOTE)
        self.assertFalse(test_event.mr_inactive)
        self.assertEqual(test_event.merge_request, body['merge_request'])
        self.assertEqual(len(test_event.labels), 20)
        self.assertTrue(all(isinstance(label, session_events.Label) for label in test_event.labels))
        self.assertEqual(test_event.project, body['project'])
        self.assertEqual(test_event.namespace, 'cki-project/kernel-ark')
        self.assertEqual(test_event.mr_url, 'https://gitlab.com/cki-project/kernel-ark/'
                                            '-/merge_requests/2757')
        self.assertIsInstance(test_event.mr_url, session_events.GitlabURL)
        self.assertEqual(test_event.bot_users, {fakes.AUTH_USER['username']})

        # GitlabNoteEvent properties
        self.assertEqual(test_event.note_text, 'request-acks-evaluation')
        self.assertEqual(test_event.noteable_type, 'MergeRequest')
        self.assertEqual(test_event.match_note_text, [])

        # BaseEvent properties
        self.assertIs(test_event.type, session_events.MessageType.GITLAB)
        self.assertEqual(test_event.rh_project.id, 13604247)
        self.assertTrue(test_event.matches_environment)
        self.assertTrue(test_event.filter_bots)
        self.assertTrue(test_event.passes_filters)
        self.assertTrue(test_event.matches_trigger)

    def test_gl_note_block(self) -> None:
        """Matches a block action."""
        class BlockTest(typing.NamedTuple):
            note_text: str
            expected_match: str

        tests = [
            BlockTest("/block this MR, I don't like it", 'block'),
            BlockTest("/unblock this MR, I'm happy now", 'unblock'),
            BlockTest("I like cake", '')
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = create_session('ack_nack')
                body = load_json_from_asset('gl_note_1.json')
                body['object_attributes']['note'] = test.note_text
                test_event = session_events.create_event(test_session, GL_HEADERS, body)
                action_matches = [match['action'] for match in test_event.match_note_text if
                                  'action' in match.groupdict()]
                if test.expected_match:
                    self.assertEqual(action_matches, [test.expected_match])
                else:
                    self.assertEqual(action_matches, [])


class TestGitlabEventFilters(TestCase):
    """Test cases for the GitlabEvent Filters."""
    # Filter functions return True when an Event passes the filter.
    # Filter functions return False when an Event fails the filter and should be ignored.

    def test_filter_drafts(self):
        """Returns False if the MR is draft and the event does not indicate that just changed."""
        class DraftsTest(typing.NamedTuple):
            passes_filter: bool
            event_body_file: str = 'gl_merge_request_1.json'
            webhook_run_on_drafts: bool = False
            body_mr_is_draft: typing.Union[bool, None] = None
            changed_previous_current: typing.Union[typing.Tuple[bool, bool], None] = None

        tests = [
            # Webhook.run_on_drafts=True, passes filter automatically.
            DraftsTest(passes_filter=True, webhook_run_on_drafts=True),
            # Webhook.run_on_drafts=False, MR is not draft and has not changes, passes.
            DraftsTest(passes_filter=True, body_mr_is_draft=False),
            # MR is draft and has not changed, fails.
            DraftsTest(passes_filter=False, body_mr_is_draft=True),
            # MR is draft but just changed, passes.
            DraftsTest(
                passes_filter=True,
                body_mr_is_draft=True,
                changed_previous_current=(False, True)
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                mock_session = mock.Mock(spec_set=['webhook'])
                mock_session.webhook.run_on_drafts = test.webhook_run_on_drafts
                body = load_json_from_asset(test.event_body_file)
                if test.body_mr_is_draft is not None:
                    mr_key = 'object_attributes' if body['object_kind'] == 'merge_request' else \
                        'merge_request'
                    body[mr_key]['draft'] = test.body_mr_is_draft
                if test.changed_previous_current:
                    body['changes']['draft'] = {'previous': test.changed_previous_current[0],
                                                'current': test.changed_previous_current[1]}
                else:
                    body.get('changes', {}).pop('draft', None)
                test_event = session_events.create_event(mock_session, GL_HEADERS, body)
                self.assertEqual(test.passes_filter, test_event.filter_drafts)
