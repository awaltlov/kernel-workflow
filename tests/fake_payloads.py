"""Basic Gitlab webhook payloads."""
from yaml import safe_load

# You better deepcopy these 😠

# Building blocks

USER_ID = 23456
USER_USERNAME = 'test_user'
USER_NAME = 'Test User'
USER_EMAIL = 'test_user@example.com'

USER_DICT = {'id': USER_ID,
             'email': USER_EMAIL,
             'name': USER_NAME,
             'username': USER_USERNAME}

BUILD_ID = 135791113
BUILD_SHA = '4a6abebc80d42d7af397c1d02443a3dceedb3e06'

PROJECT_ID = 98765
PROJECT_GROUP_ID = 65478
PROJECT_NAME = 'test_project'
PROJECT_NAMESPACE = 'group/subgroup'
PROJECT_PATH_WITH_NAMESPACE = f'{PROJECT_NAMESPACE}/{PROJECT_NAME}'
PROJECT_WEB_URL = f'https://gitlab.com/{PROJECT_PATH_WITH_NAMESPACE}'

PROJECT_DICT = {'id': PROJECT_ID,
                'default_branch': 'main',
                'description': 'a test project',
                'name': PROJECT_NAME,
                'namespace': PROJECT_NAMESPACE,
                'path_with_namespace': PROJECT_PATH_WITH_NAMESPACE,
                'web_url': PROJECT_WEB_URL}

MR_ID = 87654321
MR_IID = 123
MR_DESCRIPTION = 'An MR created for testing'
MR_HEAD_PIPELINE_ID = 7654321
MR_SOURCE_BRANCH = 'feature'
MR_TARGET_BRANCH = 'main'
MR_TITLE = 'A test MR'
MR_URL = f'{PROJECT_WEB_URL}/-/merge_requests/{MR_IID}'

MR_DICT = {'id': MR_ID,
           'iid': MR_IID,
           'action': 'update',
           'author': {'name': USER_NAME,
                      'email': USER_EMAIL
                      },
           'author_id': USER_ID,
           'created_at': '2013-12-03T17:23:34Z',
           'description': MR_DESCRIPTION,
           'draft': False,
           'head_pipeline_id': MR_HEAD_PIPELINE_ID,
           'labels': [],
           'merge_status': 'can_be_merged',
           'state': 'opened',
           'target_branch': 'main',
           'title': MR_TITLE,
           'updated_at': '2013-12-03T17:23:34Z',
           'url': MR_URL,
           'work_in_progress': False}

NOTE_ID = 768922
NOTEABLE_ID = 4235342

NOTE_DICT = {'author_id': USER_ID,
             'id': NOTE_ID,
             'description': 'Wow, this is a great MR!',
             'note': 'Wow, this is a great MR!',
             'noteable_id': NOTEABLE_ID,
             'noteable_type': 'MergeRequest',
             'project_id': PROJECT_ID,
             'type': 'DiscussionNote',
             'system': False,
             }

PIPELINE_ID = 38345686
PIPELINE_IID = 392
PIPELINE_SHA = '14a40a11a6a3d4933454c0f19811c7a2fc2f9716'
PIPELINE_STAGES = ['prepare', 'merge', 'build', 'publish', 'test']

PIPELINE_DICT = {'id': PIPELINE_ID,
                 'iid': PIPELINE_IID,
                 'detailed_status': 'passed',
                 'sha': PIPELINE_SHA,
                 'source': 'merge_request_event',
                 'stages': PIPELINE_STAGES,
                 'status': 'success',
                 'variables': []}

VAR1 = {'key': 'mr_url', 'value': MR_URL}
VAR2 = {'key': 'trigger_job_name', 'value': 'c9s_merge_request'}
VAR3 = {'key': 'mr_project_id', 'value': PROJECT_ID}

PIPELINE_DOWNSTREAM_DICT = {'id': PIPELINE_ID,
                            'iid': PIPELINE_IID,
                            'detailed_status': 'passed',
                            'sha': PIPELINE_SHA,
                            'source': 'pipeline',
                            'stages': PIPELINE_STAGES,
                            'status': 'success',
                            'variables': [VAR1, VAR2, VAR3]}

# Payloads

# https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#job-events
# AKA a job event.
BUILD_PAYLOAD = {'object_kind': 'build',
                 'build_id': BUILD_ID,
                 'build_name': 'publish x86_64',
                 'build_stage': 'publish',
                 'build_status': 'pending',
                 'commit': {},
                 'pipeline_id': MR_HEAD_PIPELINE_ID,
                 'project_id': PROJECT_ID,
                 'project_name': PROJECT_NAME,
                 'project': PROJECT_DICT,
                 'ref': 'main',
                 'repository': {},
                 'sha': BUILD_SHA,
                 'user': USER_DICT
                 }

# https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#merge-request-events
MR_PAYLOAD = {'object_kind': 'merge_request',
              'event_type': 'merge_request',
              'changes': {},
              'labels': MR_DICT['labels'],
              'project': PROJECT_DICT,
              'repository': {},
              'object_attributes': MR_DICT,
              'user': USER_DICT
              }

# https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#comment-on-a-merge-request
NOTE_PAYLOAD = {'object_kind': 'note',
                'event_type': 'note',
                'merge_request': MR_DICT,
                'object_attributes': NOTE_DICT,
                'project': PROJECT_DICT,
                'project_id': PROJECT_ID,
                'repository': {},
                'user': USER_DICT
                }

# https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#pipeline-events
PIPELINE_PAYLOAD = {'object_kind': 'pipeline',
                    'builds': [],
                    'commit': {},
                    'merge_request': MR_DICT,
                    'object_attributes': PIPELINE_DICT,
                    'project': PROJECT_DICT,
                    'source_pipeline': {},
                    'user': USER_DICT
                    }

PIPELINE_DOWNSTREAM_PAYLOAD = {'object_kind': 'pipeline',
                               'builds': [],
                               'commit': {},
                               'object_attributes': PIPELINE_DOWNSTREAM_DICT,
                               'project': PROJECT_DICT,
                               'source_pipeline': {},
                               'user': USER_DICT
                               }

PUSH_PAYLOAD = {'object_kind': 'push',
                'event_name': 'push',
                'commits': [],
                'project': PROJECT_DICT,
                'project_id': PROJECT_ID,
                'ref': 'refs/heads/main',
                'repository': {},
                'user_id': USER_ID,
                'user_name': USER_NAME,
                'user_username': USER_USERNAME,
                'user_email': USER_EMAIL
                }

BASE_MR_RESPONSE = {'currentUser': {'gid': 'gid://gitlab/User/123',
                                    'name': 'Bot User',
                                    'email': 'bot_user@example.com',
                                    'username': 'botuser'},
                    'project': {'id': f'gid://gitlab/Project/{PROJECT_ID}',
                                'userPermissions': {'pushCode': False},
                                'mr': {'approved': False,
                                       'author': {'gid': f'gid://gitlab/User/{USER_ID}',
                                                  'name': USER_NAME,
                                                  'email': USER_EMAIL,
                                                  'username': USER_USERNAME},
                                       'commitCount': 1,
                                       'description': MR_DESCRIPTION,
                                       'global_id': f'gid://gitlab/MergeRequest/{MR_IID}',
                                       'labels': {'nodes': []},
                                       'state': MR_DICT['state'],
                                       'draft': MR_DICT['work_in_progress'],
                                       'files': [{'path': 'redhat/Makefile'}],
                                       'sourceBranch': MR_SOURCE_BRANCH,
                                       'targetBranch': MR_TARGET_BRANCH,
                                       'title': MR_TITLE,
                                       'headPipeline':
                                           {'id':
                                               f'gid://gitlab/Ci::Pipeline/{MR_HEAD_PIPELINE_ID}'}
                                       }
                                }
                    }

RH_METADATA_YAML = f"""
projects:
 - name: "{PROJECT_NAME}"
   id: {PROJECT_ID}
   group_id: {PROJECT_GROUP_ID}
   pipelines: [regular]
   product: "Red Hat Enterprise Linux 9"
   webhooks:
    bughook:
     name: bughook
     instance_manages_ready_labels: true
   branches:
    - name: "{MR_TARGET_BRANCH}"
      components: ["kernel"]
      distgit_ref: "c9s"
      internal_target_release: "9.1.0"
      milestone: "RHEL-9.1.0"
webhooks:
 bughook:
  name: bughook
  instance_manages_ready_labels: true
"""

RH_METADATA = safe_load(RH_METADATA_YAML)
