"""Pipeline helper functions."""
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from enum import IntEnum
from enum import auto
from functools import cached_property
import json
from re import search as re_search
from typing import Optional
from typing import TYPE_CHECKING

from cki_lib import logger
from cki_lib.misc import get_nested_key
from gitlab import GitlabGetError
from gitlab.v4.objects import ProjectJob
from gitlab.v4.objects.pipelines import ProjectPipelineBridge
from reporter.data import CheckoutData

from . import defs

if TYPE_CHECKING:
    from gitlab.v4.objects.pipelines import ProjectPipeline
    from gitlab.v4.objects.projects import Project

LOGGER = logger.get_logger(__name__)


class PipelineType(IntEnum):
    """Types of Pipelines we might see in our Merge Requests."""

    INVALID = 0
    RHEL = auto()
    CENTOS = auto()
    ARK = auto()
    ARK_DEBUG = auto()
    ARK_16K = auto()
    ARK_16K_DEBUG = auto()
    ARK_CLANG = auto()
    ARK_CLANG_DEBUG = auto()
    ARK_CLANGLTO = auto()
    ARK_CLANGLTO_DEBUG = auto()
    RHEL_COMPAT = auto()
    REALTIME = auto()
    REALTIME_DEBUG = auto()
    AUTOMOTIVE = auto()
    _64K = auto()
    _64K_DEBUG = auto()
    CLANG = auto()
    CLANG_DEBUG = auto()
    DEBUG = auto()

    @property
    def name(self):
        # pylint: disable=function-redefined,invalid-overridden-method
        """Return the name with any leading underscores (_) removed."""
        return getattr(self, '_name_', '').removeprefix('_')

    @classmethod
    def get(cls, name):
        """Return the PipelineType that corresponds to the given job name."""
        if not name:
            return cls.INVALID
        # Try to simply match the pipe_name to the Type name.
        if ptype := next((ptype for ptype in cls if ptype.name == name.upper()), cls.INVALID):
            return ptype
        # If it doesn't have one of these endings then we don't recognize it.
        if not name.endswith(('_merge_request', '_merge_request_private')):
            return cls.INVALID
        # Now match the various types.
        return next((ptype for regex, ptype in (
            (r'_compat_', cls.RHEL_COMPAT),
            (r'_(realtime|rt)_debug_', cls.REALTIME_DEBUG),
            (r'_(realtime|rt)_', cls.REALTIME),
            (r'_automotive_', cls.AUTOMOTIVE),
            (r'_64k_debug_', cls._64K_DEBUG),
            (r'_64k_', cls._64K),
            (r'^ark_16k_debug_', cls.ARK_16K_DEBUG),
            (r'^ark_16k_', cls.ARK_16K),
            (r'^ark_clanglto_debug_', cls.ARK_CLANGLTO_DEBUG),
            (r'^ark_clanglto_', cls.ARK_CLANGLTO),
            (r'^ark_clang_debug_', cls.ARK_CLANG_DEBUG),
            (r'^ark_clang_', cls.ARK_CLANG),
            (r'^ark_debug_', cls.ARK_DEBUG),
            (r'^ark_', cls.ARK),
            (r'_clang_debug_', cls.CLANG_DEBUG),
            (r'_clang_', cls.CLANG),
            (r'_debug_', cls.DEBUG),
            (r'^(c[0-9]+s|eln)_', cls.CENTOS),
            (r'^rhel[0-9]+_', cls.RHEL),
        ) if re_search(regex, name)), cls.INVALID)

    @property
    def prefix(self):
        """Return the matching CKI label prefix, or None."""
        return CKI_LABEL_PREFIXES[self]


CKI_LABEL_PREFIXES = {PipelineType.INVALID: '',
                      PipelineType.RHEL: 'CKI_RHEL',
                      PipelineType.CENTOS: 'CKI_CentOS',
                      PipelineType.ARK: 'CKI_ARK',
                      PipelineType.ARK_DEBUG: 'CKI_ARK_Debug',
                      PipelineType.ARK_16K: 'CKI_ARK_16k',
                      PipelineType.ARK_16K_DEBUG: 'CKI_ARK_16k_Debug',
                      PipelineType.ARK_CLANG: 'CKI_ARK_Clang',
                      PipelineType.ARK_CLANG_DEBUG: 'CKI_ARK_Clang_Debug',
                      PipelineType.ARK_CLANGLTO: 'CKI_ARK_ClangLTO',
                      PipelineType.ARK_CLANGLTO_DEBUG: 'CKI_ARK_ClangLTO_Debug',
                      PipelineType.RHEL_COMPAT: 'CKI_RHEL',
                      PipelineType.REALTIME: 'CKI_RT',
                      PipelineType.REALTIME_DEBUG: 'CKI_RT_Debug',
                      PipelineType.AUTOMOTIVE: 'CKI_Automotive',
                      PipelineType._64K: 'CKI_64k',  # pylint: disable=protected-access
                      PipelineType._64K_DEBUG: 'CKI_64k_Debug',  # pylint: disable=protected-access
                      PipelineType.CLANG: 'CKI_Clang',
                      PipelineType.CLANG_DEBUG: 'CKI_Clang_Debug',
                      PipelineType.DEBUG: 'CKI_Debug'
                      }


class PipelineStatus(IntEnum):
    """Possible status of a pipeline."""

    UNKNOWN = auto()
    INVALID = auto()
    MISSING = auto()
    FAILED = auto()
    CANCELED = auto()
    RUNNING = auto()
    PENDING = RUNNING
    CREATED = RUNNING
    OK = auto()
    SUCCESS = OK

    @property
    def title(self):
        """Return capitalized name."""
        return self.name.capitalize() if self.name != 'OK' else 'OK'

    @classmethod
    def get(cls, type_str):
        """Return the PipelineStatus that matches the type_str."""
        return cls.__members__.get(type_str.upper(), cls.UNKNOWN)


@dataclass(frozen=True, kw_only=True, eq=True, order=True)
class PipelineResult:
    # pylint: disable=too-many-instance-attributes
    """Basic pipeline details."""

    # We want to be able to compare results with the same bridge name and sort them by created_at
    # date so we can find the newest instance of the bridge.
    bridge_gid: str = field(compare=False)
    bridge_name: str
    ds_pipeline_gid: str = field(compare=False)
    ds_project_gid: str
    mr_pipeline_gid: str
    ds_namespace: str
    status: PipelineStatus | str = field(compare=False)
    created_at: datetime | str
    allow_failure: bool = field(compare=False)
    stage_data: list = field(compare=False)

    def __post_init__(self):
        """Fix up the type, status, and created_at fields, if needed."""
        if isinstance(self.status, str):
            self.__dict__['status'] = PipelineStatus.get(self.status)
        if isinstance(self.created_at, str) and self.created_at:
            self.__dict__['created_at'] = datetime.fromisoformat(self.created_at[:19])
        # Flatten the stage_data a bit by removing the useless 'nodes' key.
        for stage in self.stage_data:
            if jobs := get_nested_key(stage, 'jobs/nodes'):
                stage['jobs'] = jobs
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Show yourself."""
        repr_str = f"'{self.bridge_name}' ({self.type.name}), ds ID: {self.ds_pipeline_id}"
        repr_str += f', status: {self.status.name}'
        return f'<Pipeline {repr_str}>'

    @classmethod
    def from_dict(cls, api_dict):
        """Return a new object generated from graphql data."""
        ds_pipe_dict = api_dict.get('downstreamPipeline', {})
        input_dict = {'bridge_gid': api_dict.get('id', ''),
                      'bridge_name': api_dict.get('name', ''),
                      'ds_pipeline_gid': ds_pipe_dict.get('id', ''),
                      'ds_project_gid': get_nested_key(ds_pipe_dict, 'project/id', ''),
                      'mr_pipeline_gid': get_nested_key(api_dict, 'pipeline/id', ''),
                      'ds_namespace': get_nested_key(ds_pipe_dict, 'project/fullPath', ''),
                      'stage_data': get_nested_key(ds_pipe_dict, 'stages/nodes', ''),
                      'status': ds_pipe_dict.get('status', ''),
                      'created_at': api_dict.get('createdAt', ''),
                      'allow_failure': api_dict.get('allowFailure', False),
                      }
        return cls(**input_dict)

    @property
    def bridge_id(self):
        # pylint: disable=invalid-name
        """Return the global ID of the MR bridge job as an int."""
        return int(self.bridge_gid.rsplit('/', 1)[-1]) if self.bridge_gid else 0

    @property
    def ds_pipeline_id(self):
        """Return the downstream pipeline global ID as an int."""
        return int(self.ds_pipeline_gid.rsplit('/', 1)[-1]) if self.ds_pipeline_gid else 0

    @property
    def ds_project_id(self):
        """Return the downstream project global ID as an int."""
        return int(self.ds_project_gid.rsplit('/', 1)[-1]) if self.ds_project_gid else 0

    @property
    def mr_pipeline_id(self):
        """Return the MR pipeline global ID as an int."""
        return int(self.mr_pipeline_gid.rsplit('/', 1)[-1]) if self.mr_pipeline_gid else 0

    @property
    def label(self):
        """Return the label string for pipelines of a known PipelineType, or None."""
        if not (prefix := self.type.prefix):
            return None
        if self.status is PipelineStatus.FAILED and self.allow_failure:
            return f'{prefix}::Warning'
        return f'{prefix}::{self.status.title}::{self.failed_stage.name}' if \
            self.failed_stage else f'{prefix}::{self.status.title}'

    @property
    def failed_stage(self):
        """Return the stages enum value representing the latest failed stage, or None."""
        # If the pipeline isn't failed then there is nothing to do.
        if self.status is not PipelineStatus.FAILED:
            return None
        return next((getattr(self.stages, stage['name']) for stage in reversed(self.stage_data) for
                     job in stage['jobs'] if job['status'].upper() == 'FAILED'), '')

    def get_stage(self, stage_name):
        """Return the stage data matching the given name, or None."""
        return next((stage for stage in self.stage_data if stage['name'] == stage_name), None)

    @cached_property
    def kcidb_data(self):
        """Return the KCIDB reporter CheckoutData for the pipeline, or None."""
        return CheckoutData(f'redhat:{self.ds_pipeline_id}') if self.ds_pipeline_gid else None

    @cached_property
    def stages(self):
        """Return an Enum representing the stages found in the pipeline."""
        return IntEnum('Stages', [stage['name'] for stage in self.stage_data])

    @property
    def type(self):
        """Return the PipelineType derived from the bridge job name."""
        return PipelineType.get(self.bridge_name)

    @property
    def ds_url(self):
        """Return the downstream pipeline URL, or None if it can't be derived."""
        if not self.ds_namespace or not self.ds_pipeline_gid:
            return None
        return f'{defs.GITFORGE}/{self.ds_namespace}/-/pipelines/{self.ds_pipeline_id}'

    @staticmethod
    def prepare_pipelines(input_pipelines):
        """Transform the input pipeline data into objects and filter out old instances."""
        def filter_pipes(new_pipe, existing_pipe):
            """Return whichever Pipeline is the latest, or new_pipe if existing_pipe is None."""
            if not existing_pipe:
                return new_pipe
            latest = existing_pipe if existing_pipe > new_pipe else new_pipe
            LOGGER.debug('Excluding old %s', new_pipe if latest is existing_pipe else existing_pipe)
            return latest

        # Filter out pipelines with no downstream component.
        all_pipelines = [PipelineResult.from_dict(raw_pipeline) for
                         raw_pipeline in input_pipelines if raw_pipeline['downstreamPipeline']]
        pipelines = {}
        for pipe in all_pipelines:
            pipelines[pipe.bridge_name] = filter_pipes(pipe, pipelines.get(pipe.bridge_name, None))
        return list(pipelines.values())


@dataclass
class SetupJob:
    """KCIDB Results for a pipeline setup Job."""

    job: ProjectJob
    public_project: bool

    def __repr__(self) -> str:
        """Represent yourself."""
        repr_str = f'pid: {self.job.pipeline["id"]}, jid: {self.job.id}'
        repr_str += f', {self.nvr}' if self.nvr else ', no artifact data'
        return f'<{self.__class__.__name__} {repr_str}>'

    @property
    def build_data(self) -> dict:
        """Return the first valid kcidb_all.json 'build' data dict."""
        # There are placeholder build data we ignore by only taking the build with valid = True.
        return next((bld for bld in self.kcidb_all.get('builds', []) if bld.get('valid')), {})

    @property
    def arch(self) -> str:
        """Return the build architecture."""
        return self.build_data.get('architecture', '')

    @property
    def browse_url(self) -> str:
        """Return the 'kernel_browse_url' string."""
        return next((output_file['url'] for output_file in self.build_data.get('output_files', [])
                     if output_file['name'] == 'kernel_browse_url'), '')

    @property
    def debug(self) -> bool:
        """Return True if this is a debug build, otherwise False."""
        return get_nested_key(self.build_data, 'misc/debug', False)

    @property
    def repo_url(self) -> str:
        """Return the 'kernel_package_url' string."""
        # If the build is public this URL is unique for each setup job, otherwise for private
        # projects the url for all setup jobs in the pipeline will be the same once the $basearch
        # replacement happens.
        url = next((output_file['url'] for output_file in self.build_data.get('output_files', [])
                    if output_file['name'] == 'kernel_package_url'), '')
        return url.replace(self.arch, '$basearch') if not self.public_project and self.arch else url

    @property
    def nvr(self) -> str:
        """Return the NVR for the build as a string."""
        if not self.version or not self.arch:
            return ''
        nvr = f'{self.version}.{self.arch}'
        return nvr if not self.debug else f'{nvr}-debug'

    @property
    def version(self) -> str:
        """Return the kernel_version string from the checkout data."""
        return get_nested_key(self.checkout, 'misc/kernel_version', '')

    @property
    def checkout(self) -> dict:
        """Return the checkout data from the kcidb_all.json."""
        # There is only one checkout with same data in all jobs.
        if not (checkouts := self.kcidb_all.get('checkouts')):
            return {}
        return checkouts[0]

    @cached_property
    def kcidb_all(self) -> dict:
        """Return a dict representation of the job's kcidb_all.json artifact."""
        try:
            json_file = self.job.artifact('kcidb_all.json')
        except GitlabGetError:
            LOGGER.warning('No kcidb_all.json artifact for %s', self.job)
            return {}
        return json.loads(json_file)


@dataclass
class BridgeJob:
    """Artifact data from a Bridge Job's downstream Pipeline."""

    bridge_job: ProjectPipelineBridge

    def __repr__(self) -> str:
        """Sing it."""
        repr_str = f'{self.bridge_job.name} ({self.type.name})'
        ds_pid = self._downstream_dict.get('id', 'unknown')
        repr_str += f', pid: {self.bridge_job.pipeline_id}, ds_pid: {ds_pid}'
        repr_str += f', jobs: {len(self.setup_jobs)}, valid: {self.builds_valid}'
        repr_str += f', targeted tests missing: {not self.all_sources_targeted}'
        return f'<{self.__class__.__name__} {repr_str}>'

    @property
    def _downstream_dict(self) -> dict:
        """Return the downstream_pipeline dict from the bridge_job."""
        return self.bridge_job.downstream_pipeline or {}

    @cached_property
    def ds_project(self) -> Optional['Project']:
        """Return the Gitlab Project for the downstream project, or None."""
        if not self._downstream_dict:
            return None
        return self.bridge_job.manager.gitlab.projects.get(self._downstream_dict['project_id'])

    @cached_property
    def ds_pipeline(self) -> Optional['ProjectPipeline']:
        """Return the Gitlab ProjectPipeline for the downstream pipeline, or None."""
        if not self._downstream_dict:
            return None
        return self.ds_project.pipelines.get(self._downstream_dict['id'])

    @cached_property
    def setup_jobs(self) -> list[SetupJob]:
        """Return a list of SetupJobResult objects for the given pipeline."""
        # If this is a canceled pipeline then don't even bother.
        if not self.ds_pipeline or self.ds_pipeline.status == 'canceled':
            return []
        # Get the IDs of successful 'setup' jobs.
        setup_job_ids = [job.id for job in self.ds_pipeline.jobs.list(scope='success', all=True) if
                         job.stage == 'setup']
        # "Job methods (play, cancel, and so on) are not available on ProjectPipelineJob objects.
        # To use these methods create a ProjectJob object."
        setup_jobs = [self.ds_project.jobs.get(job_id) for job_id in setup_job_ids]
        return [SetupJob(job, self.public_project) for job in setup_jobs]

    @property
    def checkout(self) -> dict:
        """Return the checkout data dict."""
        # The checkout data should be identical for every setup job in the pipeline.
        return self.setup_jobs[0].checkout if self.setup_jobs else {}

    @property
    def public_project(self) -> bool:
        """Return True if this is a public project, otherwise False."""
        # This determines how we render the artifact repo URLs.
        return self.ds_project.visibility == 'public' if self.ds_project else False

    @property
    def builds_valid(self) -> bool:
        """Return True if we have all the expected data."""
        # Verify we really have expected data for all builds, we don't want to post
        # the links to half of the jobs if artifact retrieval for some of them fails,
        # or if not all jobs were successful. That means, the number of expected
        # builds in the KCIDB data needs to match both the number of jobs and the
        # number of artifacts we actually managed to retrieve.
        return all(len(setupjob.kcidb_all.get('builds', [])) == len(self.setup_jobs) for
                   setupjob in self.setup_jobs) if self.setup_jobs else False

    @property
    def type(self) -> PipelineType:
        """Return the PipelineType for this Bridge Job."""
        return PipelineType.get(self.bridge_job.name)

    @property
    def all_sources_targeted(self) -> bool:
        """Return False if the checkout shows not all_sources_targeted, otherwise True."""
        # Default to True, if the value is not available it means we're not
        # eligible for any targeted testing.
        return get_nested_key(self.checkout, 'misc/all_sources_targeted', True)

    @property
    def version(self) -> str:
        """Return the kernel_version string from the checkout data."""
        return get_nested_key(self.checkout, 'misc/kernel_version', '')

    @property
    def artifacts_text(self) -> str:
        """Return formatted job data, if builds are valid."""
        if not self.builds_valid:
            return ''
        text = '----------------------------------------\n\n'
        text += (f'Downstream Pipeline Name: {self.type.name} ({self.bridge_job.name})\n')
        text += f'Downstream Pipeline: {self.ds_pipeline.web_url}\n'
        # For a non-public project all arches share the same repo url so it can go in the header.
        if not self.public_project:
            if repo_url := next((job.repo_url for job in self.setup_jobs if not job.debug), None):
                text += f'Repo URL: {repo_url}\n'
            if dbg_repo_url := next((job.repo_url for job in self.setup_jobs if job.debug), None):
                text += f'Debug Repo URL: {dbg_repo_url}\n'

        for setup_job in self.setup_jobs:
            text += '\n'
            text += f"{setup_job.nvr}:\n"
            text += f"Artifacts (RPMs): {setup_job.browse_url}\n"
            if self.public_project:
                text += f"Repo URL: {setup_job.repo_url}\n"
        return text
