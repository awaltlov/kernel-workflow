# Fixes Webhook

## Purpose

This webhook verifies that a Merge Request has taken all potential upstream fixes into account.

## Notifications

This webhook will report any upstream kernel Fixes: patches that should be considered for
additional backport in the Merge Request. All Fixes must either be backported or explicitly
excluded with an Omitted-Fix: tag in the MR description.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.fixes \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.
